﻿// /*******************************************************************
//  *
//  * NugetReferenceApp.cs copyright 2017 ben, 42nd Parallel - ALL RIGHTS RESERVED.
//  *
//  *******************************************************************/
using System;

using Xamarin.Forms;
using NugetReferenceLibrary;

namespace NugetReferenceApp
{
	public class App : Application
	{
		public App()
		{
			var pizza = new MyClass();

			// The root page of your application
			var content = new ContentPage
			{
				Title = "NugetReferenceApp",
				Content = new StackLayout
				{
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							HorizontalTextAlignment = TextAlignment.Center,
							Text = "Welcome to Xamarin Forms!"
						}
					}
				}
			};

			MainPage = new NavigationPage(content);
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
